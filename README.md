# JBG060-Group19
This repository contains the code for the TU/e course JBG060 Data Challenge 2. Please read this document carefully as it has been filled out with important information on how to run the code.

---

## Get Started
This guide assumes you have already python3.9.18 installed on your machine

### Install Dependencies
This readme is tested for Linux & Windows inside WSL (Windows Subsystem for Linux)
The virtual environment and python dependencies are handled py Poetry, if you don't have Poetry installed, install it by running this command in the terminal:
```
curl -sSL https://install.python-poetry.org | python3 -
```
The up-to-date commands can be found here: https://pytorch.org/get-started/locally/.


With poetry installed, to get started working on this open a terminal, type in `cd <theDirectory/youWant/toWorkin>`, then only the first time you download the repo do:

Before running `pip3 install torch` check in the *Install Pytorch* section the correct command based on your OS

```
git clone https://gitlab.com/barryboy/jbg060-dc3-g19-2023.git
cd jbg060-dc3-g19-2023/
poetry env use 3.9.18
poetry install
poetry shell
pip3 install torch
poetry run ipython kernel install --user --name=dc3
```

##### Install Pytorch
| System | GPU | Command |
|--------|---------|---------|
| Linux/WSL | NVIDIA | `pip3 install torch torchvision torchaudio` |
| Linux/WSL | CPU only | `pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu` |
| Linux | AMD | `pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/rocm5.4.2` |
| MacOS + MPS | Any | `pip3 install torch torchvision torchaudio` |
| Windows | NVIDIA | `pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu117` |
| Windows | CPU only | `pip3 install torch torchvision torchaudio` |


#### GPU Accelleration
The inference framework of choice is LLaMaCPP, a fast and lightweight framework to run LLaMa architectures. We will use the python bindings for `llama-cpp` called `llama-cpp-python`.
llama-cpp supports GPU hardware accelleration but you have to install the package with the correct CMAKE args depending on your OS.
We implemented a simple way of detecting devices to build the package accordingly in `dc3_19/llm.py`.
If you still see no GPU usage then manually run the following commands in the poetry shell:

- NVIDIA:
```
CMAKE_ARGS="-DLLAMA_CUBLAS=on" pip3 install numpy==1.25.0 tbb llama-cpp-python --force-reinstall --upgrade --no-cache-dir

```
- MPS:
```
CMAKE_ARGS="-DLLAMA_METAL=on" pip3 install numpy==1.25.0 tbb llama-cpp-python
```

*Windows Remarks:* To set the variables `CMAKE_ARGS`in PowerShell, follow the next steps (Example using, OpenBLAS):
```ps
$env:CMAKE_ARGS = "-DLLAMA_CUBLAS=on"
```
Then, call `pip` after setting the variables:
```
pip install llama-cpp-python
```

*MacOS remarks:* Detailed MacOS Metal GPU install documentation is available at https://github.com/abetlen/llama-cpp-python/blob/main/docs/install/macos.md


---
## Usage
to use the code:
```
cd <path/to>/jbg050-dc4-g19-2023/
poetry shell
python dc3_19/main.py
```

Args:
- `-c`, `--min-cluster-size`: minimum cluster size used for clustering in topic modelling, needs to be used in combination with force.
- `-f`, `--force`: force model retraining if specified, ignore cached artifacts/weights.
- `-d` `--dashboard`: if specified starts panel dashboard showcasing result df and plots.
- `-t`,`--search-topic`: specify string definition of topic to search for, to be used in combination with force.

To ensure installation was successfull you can run tests (slow for the first time since it downloads model weights), like this:
```
pytest .
```
If all test pass then installation is successfull (ignore warnings).

To start a jupyter notebook do this:
```
poetry run jupyter-lab .
```

---

## Code description
Walkthrough of code structure:

#### llm.py
Inferencing llama2 models.
contains frozen model class that given huggingface model url, it caches weights and has methods to run inference + GPU support.
The frozenmodel class will download weights from url if not present locally.
The `detect_backend` and `install_backend` methods will try to automatically detect devices and it will rebuild `llama-cpp-python` with hardware support.

Only tested with `the-bloke/` quantized .gguf model format, it supports all other sizes of models.


#### topic_modelling.py
Contains pipeline that embeds, clusters, and labels documents

Functions:
	- run\_topic\_modelling: given llm, min\_cluster\_size it returns resulting df, and other artifactsx
	- in line 62 `custom_text_gen = CustomTextGeneration(llm, prompt, n=1)` change n to change the number of representative documents to pass to the llm when generating labels for clusters

#### evaluate_topics.py
Contains pipeline that feeds topic labels to llama for filtering based on a given definition.

Functions:
	- run\_topic\_evaluation: given llm, topic: string query and definition of the topic to search for, force: necessary to set to true to run eval on custom topic.


#### vis.py
Contain embeddings visualization functions.

Function:
	- plot\_model: plots reduced embeddings with plotly, interactive, saves to .html into `imgs/embeddings_vis.html`.
	- static\_plot\_vis: plots reduced embeddings and labels top_n clusters, if topics are searched, can be used to give different shapes to isOfTopic True or False.

#### evaluate_conflicts.py
Final data processing step to get the resulting tagged articles dataset ready for timeseries forecasting usage.

Function:
	- main: merges `data/conflict_labels.csv` and `data/document_topic_mapping.csv` to format and process into final dataset of all articles with label_id, label, conflict added columns, saved to `data/articles_labelled_filtered.csv`

#### utils.py
- Contains prompts for LLM and class used to add LLaMa2 as representation_model of BERTopic object.
- `trim_to_first_n_sentences` method is used to truncate documents to `n` sentences to pass to LLM. change the defaults here to change this behaviour


#### main.py
Contain main script to run the full pipeline with given args.
Also contains the main function to start up the dashboard.


### Output
This is the list of all generated artifacts that constitute the output of the system:
- `imgs/embeddings_vis.html`: plotly interactive embeddings plot
- `imgs/static_embedding_vis.png`: mpl static embeddings plot with extra mark (circles vs diamonds) to show isOfTopic flag.
- `data/topics.csv`: list of topics with count, name, representative documents and labels generated by all different representation models (KeyBert, KeyBert+MMR, LLaMa2)
- `data/document_topic_mapping.csv`: list of docs with respective topic index + date
- `data/embeddings_cache.pkl`: embeddings python object used for caching embedding docs.
- `data/reduced_embeddings.pkl`: dimentionality reduced embeddings used for caching.
- `data/conflic_labels.csv`: same as `data/topics.csv` + boolean column of wheter or not the topic is a conflict topic.
- `data/articles_labelled_filtered.csv`: last output, docs dataset + label column + isOfTopic column.

- `models/topic_model`: trained topic model used for caching.
- `models/{llama2 version weights}`: llama2 version model weights pulled from huggingface.


## Troubleshooting
Keep in mind this repository is tested only on Linux. It should work on windows but the install procedure is not tested.
If you encounter any issues while running the notebooks, try the following:
- check that your pytorch installation is successfull
- check your Python version. In principle, the code should work with any Python 3.9 versions. If this is not the case, create a virtual environment that uses Python 3.9.18.
- check poetry documentation if poetry install fails
- check llama-cpp-python documentation for installation


#### Future improvements
- Redo the caching system and dashboard to allow faster search on different definitions.
Right now all the pipeline is rerun when specifiying a different definition, goal is to only rerun `topic_evaluation`. This on GPU should be a relatively fast search (<1min). Then add a box in the dashboard to allow live searching and visualization of the results.
- Find a way to not re-embed all the dataset whenever a user wants to add single documents
