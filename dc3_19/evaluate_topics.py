from typing import List
import os
import numpy as np
import pandas as pd
from dc3_19.llm import FrozenLlama
from dc3_19.utils import eval_prompt, system_prompt, eval_body


def load_data(path="dc3_19/data/topics.csv"):
    df = pd.read_csv(path)
    print(df.info())
    return df


def extract_results(labels: List[str], out: str)-> List[str]:
    res = []
    for label in labels:
        if label in out:
            res.append(label)
    return res


def eval_batch(labels, prompt, model):
    cleaned_labels = [s.replace("'", "").replace("[", "").replace("]", "").replace('"', '') for s in labels]
    labels_str = "- " + '\n- '.join(cleaned_labels)
    formatted_prompt = prompt.replace("{{labels}}", labels_str)
    out = model(formatted_prompt)
    return extract_results(cleaned_labels, out)


def eval_all(labels, prompt, model, n=10):
    conflict_labels = []
    for i in range(0, len(labels), n):
        labels_batch = labels[i:i+10]
        conflict_labels.append(eval_batch(labels_batch, prompt, model))
    conflict_labels = [item for sublist in conflict_labels for item in sublist]
    return conflict_labels


def add_to_df(df, conflict_labels):
    df['Llama2'] = df['Llama2'].str.replace("'", "").str.replace("[", "").str.replace("]", "").str.replace('"', '')
    df['conflict'] = df['Llama2'].isin(conflict_labels)
    df = df.drop(columns=["Topic", "Name", "Representation"])
    df.to_csv("dc3_19/data/conflict_labels.csv")
    print("saving results to dc3_19/data/conflict_labels.csv...")
    return df


def run_topic_evaluation(llm, topic=None, force=False):
    print("running topic evaluation...")
    if os.path.exists("dc3_19/data/conflict_labels.csv") and not force:
        print("loading conflict labels from cache...")
        return pd.read_csv("dc3_19/data/conflict_labels.csv")
    labels = load_data()
    if topic is None:
        conflict_labels = eval_all(labels["Llama2"], eval_prompt, llm)
    else:
        prompt = system_prompt + topic + eval_body
        conflict_labels = eval_all(labels["Llama2"], prompt, llm)
    return add_to_df(labels, conflict_labels)



if __name__ == "__main__":
    run_topic_evaluation(llm, force=True)
