from typing import List, Tuple, Mapping
from scipy.sparse.csr import csr_matrix
import pandas as pd
from bertopic.representation import BaseRepresentation
import re

def trim_to_first_n_sentences(text, n=3):
    sentences = re.split(r'(?<=[.!?])\s+', text)
    return '. '.join(sentences[:n]) + '.' if sentences else ''


class CustomTextGeneration(BaseRepresentation):
    def __init__(self, frozen_llama, prompt, n, *args, **kwargs):
        self.frozen_llama = frozen_llama
        self.prompt = prompt
        self.n = n

    def extract_topics(self,
                       topic_model,
                       documents: pd.DataFrame,
                       c_tf_idf: csr_matrix,
                       topics: Mapping[str, List[Tuple[str, float]]]
                       ) -> Mapping[str, List[Tuple[str, float]]]:
        updated_topics = {}
        for topic, keywords in topics.items():
            # Convert keywords to a single string
            keyword_str = ', '.join([word for word, _ in keywords])
            # Retrieve documents related to the topic
            topic_docs = documents[documents['Topic'] == topic]['Document'].tolist()
            trimmed_topic_docs = [trim_to_first_n_sentences(doc) for doc in topic_docs]
            docs_str = '\n- '.join(trimmed_topic_docs[:self.n])
            # Fill in placeholders
            full_prompt = self.prompt.format(documents=docs_str, keywords=keyword_str)
            # Generate text
            generated_text = self.frozen_llama(full_prompt)
            # Convert the generated_text to a format that BERTopic expects
            topic_description = [(generated_text, 1)]
            updated_topics[topic] = topic_description
            print(full_prompt+generated_text)
        return updated_topics


system_prompt = """
<s>[INST] <<SYS>>
You are a helpful, respectful and honest assistant for labeling topics.
<</SYS>>
"""

example_prompt = """
I have a topic that contains the following documents:
- Traditional diets in most cultures were primarily plant-based with a little meat on top, but with the rise of industrial style meat production and factory farming, meat has become a staple food.
- Meat, but especially beef, is the word food in terms of emissions.
- Eating meat doesn't make you a bad person, not eating meat doesn't make you a good one.

The topic is described by the following keywords: 'meat, beef, eat, eating, emissions, steak, food, health, processed, chicken'.

Based on the information about the topic above, please create a short label of this topic. Make sure you to only return the label and nothing more.
[/INST] Environmental impacts of eating meat
"""

main_prompt = f"""
[INST]
I have a topic that contains the following documents:
{{documents}}

The topic is described by the following keywords: '{{keywords}}'.

Based on the information about the topic above, please create a short label of this topic. Make sure you to only return the label and nothing more.
[/INST]
"""

prompt = system_prompt + example_prompt + main_prompt


conflict_definition = """
conflict refers to the struggle between opposing forces, which can manifest in various forms like physical, emotional, or ideological tensions. In this specific context conflicts relates to anything that can escalate into an armed confrontation between nations, states, or factions with the objective of achieving political, territorial, or ideological gains. The conflict can result in significant human, economic, and environmental repercussions.
"""

eval_body = """
[INST]
I have the following topics:
{{labels}}.

Based on the given definition of conflict, return only the labels that are related to conflict. Make sure you ONLY return the labels and nothing more, dont add any extra text.
[\INST]
"""
eval_prompt = system_prompt + conflict_definition + eval_body
