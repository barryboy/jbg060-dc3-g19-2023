import pandas as pd
from bertopic import BERTopic
import plotly.graph_objects as go
import itertools
import seaborn as sns
from matplotlib import pyplot as plt
import matplotlib.lines as mlines
from adjustText import adjust_text
import matplotlib.patheffects as pe
import textwrap


def static_vis_model(topic_model, llama2_labels, reduced_embeddings, docs, conflict_vals=None, n_topics=50, show=False):
    llama2_labels = [label[0][0].split("\n")[0] for label in topic_model.get_topics(full=True)["Llama2"].values()]
    topic_model.set_topic_labels(llama2_labels)
    colors = itertools.cycle(['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000'])
    color_key = {str(topic): next(colors) for topic in set(topic_model.topics_) if topic != -1}
    # Prepare dataframe and ignore outliers
    df = pd.DataFrame({"x": reduced_embeddings[:, 0], "y": reduced_embeddings[:, 1], "Topic": [str(t) for t in topic_model.topics_]})
    df["Length"] = [len(doc) for doc in docs]
    if conflict_vals is not None:
        df["Conflict"] = conflict_vals
        marker_map = {False: 'o', True: 'D'}  # Map boolean to shape
        df['Marker'] = df['Conflict'].map(marker_map)
        unique_markers = df['Marker'].unique()
        marker_dict = {marker: marker for marker in unique_markers}

    df = df.loc[df.Topic != "-1"]
    df = df.loc[(df.y > -10) & (df.y < 10) & (df.x < 10) & (df.x > -10), :]
    df["Topic"] = df["Topic"].astype("category")
    # Get centroids of clusters
    mean_df = df.groupby("Topic").mean().reset_index()
    mean_df.Topic = mean_df.Topic.astype(int)
    mean_df = mean_df.sort_values("Topic")
    fig = plt.figure(figsize=(20, 20))
    if conflict_vals is not None:
        sns.scatterplot(data=df, x='x', y='y', hue='Topic', style='Marker', markers=marker_dict,
                        palette=color_key, alpha=0.6, sizes=(1, 100), size="Length", legend="full")
        # Create custom handles for legend
        handle_list = []

        handle_list.append(mlines.Line2D([0], [0], linestyle='none', marker='o', color='black', markersize=10, label='Conflict=False'))
        handle_list.append(mlines.Line2D([0], [0], linestyle='none', marker='D', color='black', markersize=10, label='Conflict=True'))
        plt.legend(handles=handle_list, title='Legend', bbox_to_anchor=(1.05, 1), loc='upper left')
    else:
        sns.scatterplot(data=df, x='x', y='y', c=df['Topic'].map(color_key), alpha=0.4, sizes=(0.4, 10), size="Length")
    # Annotate top n_topics topics
    texts, xs, ys = [], [], []
    for row in mean_df.iterrows():
        topic = row[1]["Topic"]
        name = textwrap.fill(topic_model.custom_labels_[int(topic)], 20)
        if int(topic) <= n_topics:
            xs.append(row[1]["x"])
            ys.append(row[1]["y"])
            texts.append(plt.text(row[1]["x"], row[1]["y"], name, size=10, ha="center", color=color_key[str(int(topic))],
                          path_effects=[pe.withStroke(linewidth=0.5, foreground="black")]
                          ))
    # Adjust annotations such that they do not overlap
    adjust_text(texts, x=xs, y=ys, time_lim=1, force_text=(0.01, 0.02), force_static=(0.01, 0.02), force_pull=(0.5, 0.5))
    plt.axis('off')
    plt.legend('', frameon=False)
    plt.savefig("imgs/static_embeddings_vis.png")
    if show:
        plt.show()
    return fig


def vis_model(topic_model, labels, reduced_embeddings, df, show=False):
    topic_model.set_topic_labels(labels)
    fig = topic_model.visualize_documents(df["summary"], reduced_embeddings=reduced_embeddings, hide_annotations=True, hide_document_hover=False, custom_labels=True)
    fig.write_html("imgs/embeddings_vis.html")
    if show:
        fig.show()
    return fig
