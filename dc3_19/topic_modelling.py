from bertopic import BERTopic
from bertopic.representation import KeyBERTInspired, MaximalMarginalRelevance, TextGeneration
import transformers
from umap import UMAP
from hdbscan import HDBSCAN
from sentence_transformers import SentenceTransformer
import numpy as np
import pandas as pd
import os
import pickle

from dc3_19.llm import FrozenLlama
from dc3_19.utils import CustomTextGeneration
from dc3_19.vis import static_vis_model, vis_model
from utils import prompt


def load_data(path="dc3_19/data/articles_summary_cleaned.csv"):
    df = pd.read_csv(path, parse_dates=["date"]) # Read data into 'df' dataframe
    print("Articles summary cleaned csv", df.info())
    docs = df["summary"].tolist()
    return df, docs


def train_model(topic_model, docs, embeddings, force=False):
    artifacts = ["dc3_19/data/topics.csv" ,"dc3_19/models/topic_model"]
    print("Training topic model...")
    topics, probs = topic_model.fit_transform(docs, embeddings)
    print(f"Generated clusters...")
    df = topic_model.get_topic_info()
    llama2_labels = [label[0][0].split("\n")[0] for label in topic_model.get_topics(full=True)["Llama2"].values()]
    # Save the results
    print(f"saving resulting topics at {artifacts[0]}")
    df.to_csv(artifacts[0], index=False)
    print(f"saving trained model weights at {artifacts[1]}")
    topic_model.save(artifacts[1])
    return df, llama2_labels, topic_model, topics


def train_embeddings(data):
    embedding_model = SentenceTransformer("BAAI/bge-small-en")
    print("Embedding documents...")
    embeddings = embedding_model.encode(data, show_progress_bar=True)
    return embeddings, embedding_model


def load_model(llm, data, min_cluster_size):
    embeddings, embedding_model = train_embeddings(data)
    reduced_embeddings = reduce_embeddings(embeddings)
    with open("dc3_19/data/embeddings_cache.pkl", "wb") as f:
        print("saving embeddings at dc3_19/data/embeddings_cache.pkl")
        pickle.dump(embeddings, f)
    with open("dc3_19/data/reduced_embeddings_cache.pkl", "wb") as f:
        print("saving reduced embeddings at dc3_19/data/reduced_embeddings_cache.pkl")
        pickle.dump(reduced_embeddings, f)
    umap_model = UMAP(n_neighbors=15, n_components=5, min_dist=0.0, metric='cosine',
                      random_state=42)
    hdbscan_model = HDBSCAN(min_cluster_size=min_cluster_size, metric='euclidean',
                            cluster_selection_method='eom', prediction_data=True)
    keybert = KeyBERTInspired()
    mmr = MaximalMarginalRelevance(diversity=0.6)
    custom_text_gen = CustomTextGeneration(llm, prompt, n=1)

    representation_model = {
        "KeyBERT": keybert,
        "Llama2": custom_text_gen,
        "MMR": mmr}

    topic_model = BERTopic(
        # Sub-models
        embedding_model=embedding_model,
        umap_model=umap_model,
        hdbscan_model=hdbscan_model,
        representation_model=representation_model,
        # Hyperparameters
        top_n_words=10,
        verbose=False)
    return topic_model, embeddings, reduced_embeddings


def run_model(llm, docs, min_cluster_size):
        topic_model, embeddings, reduced_embeddings = load_model(llm, docs, min_cluster_size)
        df, llama2_labels, topic_model, topics = train_model(topic_model, docs, embeddings)
        return df, embeddings, llama2_labels, topic_model, topics, reduced_embeddings


def run_topic_modelling(llm: FrozenLlama, min_cluster_size: int, force=False):
    print("running topic modelling")
    artifacts = ["dc3_19/data/topics.csv", "dc3_19/data/document_topic_mapping.csv", "dc3_19/models/topic_model", "dc3_19/data/embeddings_cache.pkl", "dc3_19/data/reduced_embeddings_cache.pkl"]
    df, docs = load_data()

    if all(os.path.exists(path) for path in artifacts) and not force:
        print("loading topics, topic mapping csvs and topic model from cache....")
        # cached topics & topic mapping csvs, just run vis
        df_results = pd.read_csv(artifacts[0])
        df_mapping = pd.read_csv(artifacts[1])
        topic_model = BERTopic.load(artifacts[2])
        llama2_labels = [label[0][0].split("\n")[0] for label in topic_model.get_topics(full=True)["Llama2"].values()]
        with open(artifacts[4], "rb") as f:
            reduced_embeddings = pickle.load(f)
    else:
        # run run_model to create df_reswults, topics, df_mapping
        df_results, embeddings, llama2_labels, topic_model, topics, reduced_embeddings = run_model(llm, docs, min_cluster_size)
        df_mapping = pd.DataFrame({"Document": docs, "Topic": topics, "Date": df["date"]})
        df_results.to_csv(artifacts[0], index=False)
        df_mapping.to_csv(artifacts[1], index=False)

    plotly_fig = vis_model(topic_model, llama2_labels, reduced_embeddings, df, show=False)
    mpl_fig = static_vis_model(topic_model, llama2_labels, reduced_embeddings, docs, show=False)
    models = {"topic_model": topic_model, "llama2_labels": llama2_labels, "reduced_embeddings": reduced_embeddings, "docs": docs}
    return df_results, df_mapping, plotly_fig, mpl_fig, models


def reduce_embeddings(embeddings, force=False):
    print("reducing embeddings dimensionality...")
    reduced_embeddings = UMAP(n_neighbors=15, n_components=2, min_dist=0.0, metric='cosine', random_state=42).fit_transform(embeddings)
    print("oen:")
    return reduced_embeddings




if __name__ == "__main__":
    run_topic_modelling(llm, min_cluster_size, force=True)
