import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random


def custom_interpolate_with_gap(s, gap_size):
    arr = s.to_numpy()
    mask = np.isnan(arr)
    idx = np.where(mask)[0]

    if len(idx) == 0:
        return s

    diff = np.diff(idx)
    gaps = np.where(diff > 1)[0]
    starts = np.zeros(gaps.shape[0] + 1, dtype=np.int64)
    ends = np.zeros(gaps.shape[0] + 1, dtype=np.int64)

    starts[0] = idx[0]
    ends[-1] = idx[-1]

    for i in range(gaps.shape[0]):
        starts[i + 1] = idx[gaps[i] + 1]
        ends[i] = idx[gaps[i]]

    for start, end in zip(starts, ends):
        if end - start <= gap_size:
            if end + 1 >= len(arr):
                end_value = arr[end]
            else:
                end_value = arr[end + 1]
            interpolated_values = np.linspace(arr[start-1], end_value, end-start+1)
            arr[start:end+1] = interpolated_values

    return pd.Series(arr, index=s.index)


def interpolate_and_plot_random_n_groups(df, column_name, gap_size, n=3):
    initial_nans = df[column_name].isna().sum()
    # Randomly select n districts
    unique_districts = df['district'].unique()
    random_districts = random.sample(list(unique_districts), min(n, len(unique_districts)))
    # Filter the DataFrame to only include the random districts
    df_filtered = df[df['district'].isin(random_districts)]

    # Plot before interpolation
    plt.subplot(1, 2, 1)
    nan_percentage1 = df[column_name].isna().mean() * 100
    for district, group in df_filtered.groupby('district'):
        plt.plot(group.index, group[column_name], marker='o', label=f"{district} (Before)")
    plt.title(f'Before Interpolation for {column_name}, {nan_percentage1}% nans')
    plt.xlabel('Timestamp')
    plt.ylabel('Value')
    plt.legend()

    # Apply custom interpolation function per district
    for district, group in df_filtered.groupby('district'):
        interpolated_values = custom_interpolate_with_gap(group[column_name], gap_size=gap_size)
        df_filtered.loc[df_filtered['district'] == district, column_name] = interpolated_values
    final_nans = df_filtered[column_name].isna().sum()
    filled_nans = initial_nans - final_nans

    # Plot after interpolation
    plt.subplot(1, 2, 2)
    nan_percentage2 = df[column_name].isna().mean() * 100
    for district, group in df_filtered.groupby('district'):
        plt.plot(group.index, group[column_name], marker='o', label=f"{district} (After)")
    plt.title(f'{filled_nans} filled nans for {column_name}, {nan_percentage2}%')
    plt.xlabel('Timestamp')
    plt.ylabel('Value')
    plt.legend()
    plt.tight_layout()
    plt.show()



data_dir = "dc3_19/data/"
df = pd.read_csv(data_dir + "food_crises_cleaned.csv") # Read data into DataFrame
df["date"] = pd.to_datetime(df["year_month"], format="%Y_%m") # Create date column
print(len(df['district'].unique()))
df.set_index(["date"], inplace=True) # Set index
print(df.info(), df.head())

#df.reset_index(level='district', inplace=True)
interpolate_and_plot_random_n_groups(df, 'ipc', 2)
