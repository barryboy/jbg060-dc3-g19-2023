import panel as pn
import plotly.express as px
from dc3_19.topic_modelling import run_topic_modelling
from dc3_19.evaluate_topics import run_topic_evaluation
from dc3_19.evaluate_conflicts import main as run_evaluate_conflicts
from dc3_19.llm import FrozenLlama
from dc3_19.vis import static_vis_model
import argparse
import pandas as pd
import matplotlib.pyplot as plt

# from main import main_dash


def parser():
    p = argparse.ArgumentParser(prog="JBG060",
                                description="Launch DC3 full experiment",
                                epilog="Made with love by group 19")
    p.add_argument("-c", "--min-cluster-size",
                   help="set minimum cluster size",
                   default=50, type=int)
    p.add_argument("-f", "--force",
                   help="force model retraining if cached weights are present",
                   action="store_true")
    p.add_argument("-d", "--dashboard",
                   help="if specified enables dashboard",
                   action="store_true")
    p.add_argument("-t", "--search-topic",
                   help="definition of topic to prompt to LLM to search.",
                   default=None, type=str)
    return p.parse_args()


def main_dash(plotly_fig, mpl_fig, df, df_conflict):


    df_conflict = df_conflict[["Document", 'Date', 'Llama2', 'conflict']]

    pn.extension('plotly', design='material')
    
    # Update plotly figure layout
    plotly_fig.update_layout(autosize=True, width=1000, height=800)
    dropdown = pn.widgets.Select(name='Select column', options=df.columns.tolist())
    
    # create panes and tabs for plots
    plotly_pane = pn.pane.Plotly(plotly_fig, height=500, sizing_mode='stretch_width', align='center')
    mpl_pane = pn.pane.Matplotlib(mpl_fig, tight=False, width=1000, height=1000, align='center')

    plot_tabs = pn.Tabs(('Clusters - Topic Names in Plot', mpl_pane), ('Clusters - Topic Name in Legend', plotly_pane))
    
    # create dataframe pane
    table_data = df_conflict
    table = pn.widgets.DataFrame(table_data, width=1000, height=800)
    table_tabs = pn.Tabs(('Table', table))
    
    # create sidebar options
    sort_options = pn.widgets.Select(name='Conflict articles', options=['All', 'Yes', 'No'])
    filter_options = pn.widgets.TextInput(name='Filter by name', value='')
    
    
    df_conflict_copy = df_conflict.copy()

    # Define callback function for sort options
    def update_sort(option):
        if option == 'All':
            table.value = df_conflict_copy
        elif option == 'Yes':
            table.value = df_conflict_copy[df_conflict_copy['conflict'] == 'true']
        elif option == 'No':
            table.value = df_conflict_copy[df_conflict_copy['conflict'] == 'false']

    # Link sort options to callback function
    sort_options.link(update_sort, value='value')

    # Define callback function for filter options
    def update_filter(value):
        if value == '':
            table.value = df_conflict_copy
        else:
            table.value = df_conflict_copy[df_conflict_copy['name'].str.contains(value, case=False)]

    # Link filter options to callback function
    filter_options.link(update_filter, value='value')
        
    # Create sidebar
    sidebar = pn.Column(
        pn.pane.Markdown("## Sidebar", margin=(0, 10)),
        sort_options,
        filter_options,
        margin=(0, 10)
    )
    
    # Create layout
    dashboard = pn.template.FastListTemplate(
        title='DC3 Dashboard - Group 19',
        header_background='#6f42c1',
        main=[
            pn.Tabs(
                ('Plots', pn.Row(
                    pn.Column(
                        dropdown,
                        plot_tabs,
                        margin=(0, 10)
                    ),
                    sizing_mode='stretch_width'
                )),
                
                ('Table', pn.Row(
                    pn.Column(
                        table_tabs,
                        margin=(0, 10),
                        
                    ),
                    sizing_mode='stretch_width'
                )
                )),
            ],
            sidebar=sidebar
        )
    


    
    # Show dashboard
    dashboard.show()


def main():
    args = vars(parser())
    llm = FrozenLlama("https://huggingface.co/TheBloke/Llama-2-7b-Chat-GGUF/resolve/main/llama-2-7b-chat.Q3_K_M.gguf", .1, max_tokens=2048, n_gpu_layers=40, verbose=False)
    df, topic_mapping_df, plotly_plt, mpl_plt, models = run_topic_modelling(llm, args["min_cluster_size"], force=args["force"])
    conflict_df = run_topic_evaluation(llm, force=args["force"], topic=args["search_topic"])
    final_df = run_evaluate_conflicts(force=args["force"])

    mpl_plt = static_vis_model(models["topic_model"], models["llama2_labels"], models["reduced_embeddings"], models["docs"], final_df["conflict"], 40)
    if args["dashboard"]:
        main_dash(plotly_plt, mpl_plt, df, final_df)


if __name__ == "__main__":
    main()
