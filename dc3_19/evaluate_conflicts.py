import numpy as np
import pandas as pd
from pathlib import Path
import os
from dc3_19.vis import static_vis_model




def load_data(path=Path("dc3_19/data")):
    df_labels = pd.read_csv(path / "conflict_labels.csv")
    df_articles = pd.read_csv(path / "document_topic_mapping.csv")

    df_labels = df_labels.drop(columns=["Unnamed: 0"])
    df_labels['label_id'] = df_labels.index - 1
    return df_labels, df_articles


def merge_all(df_articles, df_labels):
    df_merged = pd.merge(df_articles, df_labels[['label_id', 'Llama2', 'conflict']],
                               left_on='Topic', right_on='label_id', how='left')
    # Drop the redundant 'label_id' column
    df_merged.drop(columns=['label_id'], inplace=True)
    return df_merged


def main(force=False):
    if os.path.exists("dc3_19/data/articles_labelled_filtered.csv") and not force:
        print("loading results dataset from cache...")
        return pd.read_csv("dc3_19/data/articles_labelled_filtered.csv")
    df_labels, df_articles = load_data()
    df_final = merge_all(df_articles, df_labels)
    df_final.to_csv("dc3_19/data/articles_labelled_filtered.csv")
    print("Final articles dataset with label and conflict bool created. saving at dc3_19/data/articles_labelled_filtered.csv")
    print(df_final.info(), df_final.head())
    return df_final


if __name__ == "__main__":
    main(force=True)
