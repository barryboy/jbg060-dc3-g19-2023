from typing import Dict
from pathlib import Path
import wget
import os
import subprocess
import torch
from langchain.llms import LlamaCpp
from langchain import PromptTemplate, LLMChain
from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler


class FrozenLlama:
    def __init__(self, url: str, temperature=0.1, max_tokens=512, n_batch=512, n_gpu_layers=40, seed=69, top_p=1, verbose=False):
        self.template: str = """[INST] <<SYS>> You are a talented and honest assistant and scientist.
        If you don't know the answer to a question, please don't share false information.<</SYS>> {prompt} [/INST]"""
        self.weights: Path = self.download_llama_model_if_missing(url)
        self.temperature = temperature
        self.max_tokens = max_tokens
        self.n_batch = n_batch
        self.n_gpu_layers = n_gpu_layers
        self.seed = seed
        self.top_p = top_p
        self.verbose = verbose

        self.callback = None
        self.llama = None
        self.prompt = None
        self.llm_chain = None

    def detect_backend(self) -> None:
        try:
            os.mknod('dc3_19/data/.lock_gpu')
            device = "mps" if torch.backends.mps.is_available() else "cpu"
            device = "cuda" if torch.cuda.is_available() else device
            if device != "cpu":
                print("{device} device detected, installing extras for hardware accelleration...")
                self.install_backend(device)
        except FileExistsError:
            pass

    def install_backend(self, device):
        if device == "cuda":
            subprocess.run([
                "CMAKE_ARGS='-DLLAMA_CUBLAS=on' pip3 install numpy==1.25.0 tbb llama-cpp-python --force-reinstall --upgrade --no-cache-dir"
            ], shell=True)
        elif device == "mps":
            print("install on MPS is not tested, you are on your own")
            subprocess.run([
                "CMAKE_ARGS='-DLLAMA_METAL=on' pip3 install numpy==1.25.0 tbb llama-cpp-python --force-reinstall --upgrade --no-cache-dir"
            ])

    def load_model(self) -> None:
        print("Loading LLaMa2 model...")
        if self.n_gpu_layers > 0:
            self.detect_backend()
        self.callback: CallbackManager = CallbackManager([StreamingStdOutCallbackHandler()])
        self.llama: LlamaCpp = LlamaCpp(
            model_path=str(self.weights),
            temperature=self.temperature,
            n_gpu_layers=self.n_gpu_layers,
            n_batch=self.n_batch,
            max_tokens=self.max_tokens,
            top_p=self.top_p,
            callback=self.callback,
            seed=self.seed,
            verbose=self.verbose)

    def load_llm_chain(self):
        self.prompt: PromptTemplate = self.create_prompt()
        self.llm_chain: LLMChain = LLMChain(llm=self.llama, prompt=self.prompt)

    def create_prompt(self) -> PromptTemplate:
        self.prompt: PromptTemplate = PromptTemplate(
            input_variables=["prompt"],
            template=self.template)
        return self.prompt

    def download_llama_model_if_missing(self, url: str) -> Path:
        path = Path("dc3_19/models/") / Path(url.split("/")[-1])
        if path.exists():
            print(f"using cached model: {path}")
            return path
        print(f"cached model not found, downloading")
        path.parent.mkdir(parents=True, exist_ok=True)
        try:
            wget.download(url, str(path))
        except Exception as e:
            print(f"An error occurred while downloading: {e}")
            return None
        print(f"File downloaded to {path}")
        return path

    def query(self, query: str) -> str:
        if self.llm_chain is None:  # Check if the model is loaded
            self.load_model()
            self.load_llm_chain()
        return self.llm_chain.run(query)

    def __call__(self, query: str)-> Dict:
        if self.llama is None:  # Check if the model is loaded
            self.load_model()
        return self.llama(query)


if __name__ == "__main__":
    url = "https://huggingface.co/TheBloke/Llama-2-7b-Chat-GGUF/resolve/main/llama-2-7b-chat.Q3_K_M.gguf"
    llm = FrozenLlama(
        url=url,
        temperature=0.1,
        max_tokens=1024,
        n_batch=512,
        n_gpu_layers=40,
        seed=69,
        top_p=1,
        verbose=True)

    q = "tell me the short list of planets of the solar system"
    response = llm.forward(q)
    print(response)
