import pytest
from dc3_19.llm import FrozenLlama
from dc3_19.utils import CustomTextGeneration, prompt
from scipy.sparse import csr_matrix
import pandas as pd


@pytest.fixture(scope="module")
def llm_model():
    url = "https://huggingface.co/TheBloke/Llama-2-7b-Chat-GGUF/resolve/main/llama-2-7b-chat.Q3_K_M.gguf"
    llm = FrozenLlama(
        url=url,
        temperature=0.1,
        max_tokens=1024,
        n_batch=512,
        n_gpu_layers=40,
        seed=69,
        top_p=1,
        verbose=True)
    return llm

def test_sanity_check_llama(llm_model):
    planets = ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"]
    q = "tell me the short list of planets of the solar system"
    response = llm_model.query(q)
    assert all(planet in response for planet in planets), f"Model sanity check failed: {response}"

def test_sanity_check_raw_llama(llm_model):
    planets = ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"]
    q = """[INST] <<SYS>> You are a talented and honest assistant and scientist.
        If you don't know the answer to a question, please don't share false information.<</SYS>> tell me the short list of planets of the solar system [/INST]"""
    response = llm_model(q)
    assert all(planet in response for planet in planets), f"Model sanity check failed: {response}"

def test_CustomTextGeneration(llm_model):
    custom_gen = CustomTextGeneration(llm_model, prompt, 3)
    # Mock data
    mock_docs = pd.DataFrame({
        'Document': ['doc1', 'doc2'],
        'Topic': [0, 1]
    })
    mock_c_tf_idf = csr_matrix([[1, 0], [0, 1]])
    mock_topics: Mapping[str, List[Tuple[str, float]]] = {
        0: [('word1', 0.3), ('word2', 0.7)],
        1: [('word3', 0.2), ('word4', 0.8)]
    }
    # Call extract_topics
    updated_topics = custom_gen.extract_topics(None, mock_docs, mock_c_tf_idf, mock_topics)
    # Assertions
    assert isinstance(updated_topics, dict)
    assert len(updated_topics) == len(mock_topics)
    for topic, topic_desc in updated_topics.items():
        assert len(topic_desc) >= 1  # Assuming at least one description is generated
        for desc, weight in topic_desc:
            assert isinstance(desc, str)
            assert isinstance(weight, (int, float))
